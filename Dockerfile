FROM openjdk:8
ADD build/libs/wallboard-0.0.1-SNAPSHOT.jar wallboard-0.0.1-SNAPSHOT.jar
EXPOSE 8093
ENTRYPOINT ["java","-jar","wallboard-0.0.1-SNAPSHOT.jar"]
