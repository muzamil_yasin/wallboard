package com.asterisk.wallboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class WallboardAsteriskApplication {

	public static void main(String[] args) {
		SpringApplication.run(WallboardAsteriskApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello() {
        	return "Hello World RESTful with Spring Boot";
    }  

}
